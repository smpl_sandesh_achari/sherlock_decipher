# SEDEMAC Proprietary License

Copyright (c) 2018, `SEDEMAC Mechatronics Pvt. Ltd`
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, is permitted to be used by SEDEMAC employees authorized access. The software *SHOULD NOT* be used for personal projects of developers. The distribution is not permitted to be copied on any laptop/desktops outside of those managed by SEDEMAC IT infrastructure.



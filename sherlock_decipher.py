################################################################################################
# Copyright (C) 2016, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.
#
# Module name: eeprom_access.py
# Author: Sandesh Achari
# Date created: 22-Oct-2021
# Module description: This file communicates with EEPROM. It performs following functionality.
#						1. Erase EEPROM
#						2. Extract data from EEPROM
# Module dependency: pycan package should be installed before using this script. Check pycan's 
#	bitbucket repository - https://bitbucket.org/sedemac/pycan/src/master/
#
################################################################################################



import xlsxwriter
from pycan import *
from pycan.can_msg import *
from pycan.comm import *
import os
import sys
import time

BAUD_RATE_1M = 1000000
BAUD_RATE_500K = 500000
BAUD_RATE = BAUD_RATE_1M


database = { 'READ_START_ID':int('0x330', 16), 
			'HEADER_ID':int('0x331', 16),
			'COUNTER_ID':int('0x332', 16),
			'EVENT_LOG_ID':[int('0x333', 16), int('0x334', 16), int('0x335', 16), int('0x336', 16)],
			'TIME_TRACE_ID':[int('0x337', 16), int('0x338', 16), int('0x339', 16), int('0x33A', 16)],
			'READ_STOP_ID':int('0x33B', 16) }


'''
This function is used to transmit CAN message as per the input command. It supports following commands.
1. Admin login
2. Extract data from EEPROM
3. Erase EEPROM
4. Logout from admin mode 
'''
def SendMsg(cmd):
	cmdValid = True
	if(cmd == 'Admin Login'):
		id = int('0x346', 16)
		data_bytes = [int('0x11', 16), int('0x23', 16), int('0x35', 16), int('0x47', 16), int('0x59', 16), int('0x61', 16), int('0x73', 16), int('0x85', 16)]
	elif(cmd == 'Extract Data'):
		id = int('0x345', 16)
		data_bytes = [int('0x11', 16), int('0x23', 16), int('0x35', 16), int('0x47', 16), int('0x59', 16), int('0x61', 16), int('0x73', 16), int('0x85', 16)]
	elif(cmd == 'Erase EEPROM'):
		id = int('0x352', 16)
		data_bytes = [int('0x21', 16), int('0x33', 16), int('0x45', 16), int('0x57', 16), int('0x69', 16), int('0x7A', 16), int('0x8C', 16), int('0x9E', 16)]
	elif(cmd == 'Logout'):
		id = int('0x347', 16)
		data_bytes = [int('0xFF', 16), int('0xFF', 16), int('0xFF', 16), int('0xFF', 16), int('0xFF', 16), int('0xFF', 16), int('0xFF', 16), int('0xFF', 16)]
	else:
		cmdValid = False
		print('CAN message cannot be transmitted. Incorrect option provided.')

	if cmdValid:
		msg = CanMsg(message_number=1, time=0, bus=1, rx_tx_type="Tx", identifier=id, data_length=8, data_bytes=data_bytes)
		send(msg)	

def WriteHexToText(txtFileObj, write_data):
	for i in (range(len(write_data))):
		for j in range(len(write_data[i])):
			# Convert string into capital letters
			s = hex(write_data[i][j]).upper()
			# If there is single digit/letter in hex number then add 0 before it
			if(len(s) == 3):
				s = '0' + s[-1]
			else:
				s = s[2:len(s)]
			txtFileObj.write(s + " ")
		txtFileObj.write("\n")



'''
To do...
'''
def WriteToExcel(excelFileObj, data):
	pass




'''
To erase EEPROM, pass 'erase' as an argument.
To extract data from EEPROM, pass 'extract_data' as an argument.
'''
if __name__ == '__main__':
    pycan.comm.terminate()
    pycan.comm.init(BAUD_RATE)
    command = sys.argv[1]

    if command == 'extract_data':
    	filePath = os.getcwd()
    	getDateTime = datetime.datetime.now().strftime("_%d%b%Y_%H_%M_%S")
    	txtFileName = "EEPROM_data" + getDateTime + ".txt"
    	headerId = database['HEADER_ID']
    	diagDataId = database['COUNTER_ID']
    	eventLogId = database['EVENT_LOG_ID']
    	timeTraceId = database['TIME_TRACE_ID']
    	readStopId = int(database['READ_STOP_ID'])
    	headerList, diagDataList, timeTraceList, eventLogList = [], [], [], []

    	SendMsg('Extract Data')    	
    	print("Fetching data from EEPROM. This should not take more than 1.5 minutes.")

    	startTime_sec = time.time()

    	msgCnt = 0
    	identifier = 0
    	while(identifier != readStopId):
    		message = recv()
    		if(message is not None):
    			msgCnt = 0
    			identifier= message.identifier
    			if(message.identifier == (int(headerId))):
    				headerList.append(message.data_bytes)
    			elif(message.identifier == (int(diagDataId))):
    				diagDataList.append(message.data_bytes)
    			elif(message.identifier in timeTraceId):
    				timeTraceList.append(message.data_bytes)
    			elif(message.identifier in eventLogId):
    				eventLogList.append(message.data_bytes)
    			elif(message.identifier == readStopId):
    				break;
    			msgCnt = msgCnt + 1
    		if(msgCnt >= 1500):
    			print("Timeout!")
    			break;

    	txtFileObj = open(txtFileName,'w')
    	# Write Header data
    	txtFileObj.write("Header: " + hex(int(headerId)) + '\n')
    	WriteHexToText(txtFileObj, headerList)
    	# Write Diagnostic data
    	txtFileObj.write("\n\nDiagnostic data: " + hex(int(diagDataId)) + '\n')
    	WriteHexToText(txtFileObj, diagDataList)
    	# Write Event Log data
    	txtFileObj.write("\n\nEvent log: " + hex(int(eventLogId[0])) + ' ' + hex(int(eventLogId[1])) + ' ' + hex(int(eventLogId[2])) + ' ' + hex(int(eventLogId[3])) + '\n')
    	WriteHexToText(txtFileObj, eventLogList)
    	# Write Time Trace data
    	txtFileObj.write("\n\nTime trace: " + hex(int(timeTraceId[0])) +' ' + hex(int(timeTraceId[1])) + ' ' + hex(int(timeTraceId[2])) + ' ' + hex(int(timeTraceId[3])) + '\n')
    	WriteHexToText(txtFileObj, timeTraceList)
    	txtFileObj.close()
    	print('Text file ' + filePath + "\\" + txtFileName + ' created.')
    	print('Total time taken to extract data from EEPROM: ', int(time.time() - startTime_sec), 'seconds.')
    elif command == 'erase':
    	SendMsg('Admin Login')
    	SendMsg('Erase EEPROM')
    	eraseAckCnt = 0
    	startTime_sec = time.time()
    	while((time.time() - startTime_sec) < 10):
    		msg = recv()
    		if(msg is not None):
	    		if(msg.identifier == int('0x400', 16)):
	    			startTime_sec = time.time()
	    			eraseAckCnt = eraseAckCnt + 1
	    			if(eraseAckCnt == 129):
	    				if (msg.data_bytes[0] == int('0xAA', 16) and
	    					msg.data_bytes[1] == int('0x55', 16) and 
	    					msg.data_bytes[2] == int('0xAA', 16) and 
	    					msg.data_bytes[3] == int('0x55', 16) ):
	    					print('EEPROM erased successfully.')
	    				else:
	    					print('EEPROM erase failed.')
	    				break
    				else:
    					print('Erasing sector:', eraseAckCnt, end='\r')
    	if(eraseAckCnt < 129):
    		print('\nErase failed.')
    	# SendMsg('Logout')
    else:
    	print('Incorrect option given. Valid options are \'erase\', \'extract_data\'. ')


    pycan.comm.terminate()


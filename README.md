# Sherlock Decipher

This repository shall be used to read/erase Sherlock data in EEPROM. Use of this repository shall be limited to only ISG Gen-3 projects. 

## Dependency
1. pycan - Install this package from repository https://bitbucket.org/sedemac/pycan/src/master/


## How to use
* To erase EEPROM, run ```python sherlock_decipher.py erase```.
* To extract data from EEPROM, run ```python sherlock_decipher.py extract_data```. Text file having the retrieved EEPROM data will be generated in the same location.

## Contributors
* Sandesh Achari
